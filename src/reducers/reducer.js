import { combineReducers } from 'redux'
import terminalHistory from './terminalHistory.js'
import game from './game.js'

const reducer = combineReducers({
  terminalHistory,
  game
})

export default reducer
